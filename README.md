![T-Rex](t_rex_typer/resources/trex_w_board_96.png)

# T-Rex Typer
A desktop application to improve steno writing.

![The T-Rex Typer](t_rex_typer/resources/screenshot.png)

# Install
The T-Rex Typer was developed using GNU/Linux with Python3.9 although
it may work with other operating systems using Python>=3.6.

```python
pip install t_rex_typer
```

Run with

```t_rex_typer```

or

```python -m t_rex_typer```
