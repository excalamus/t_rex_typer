# test_tab_safe_line_edit.py
#
# Run tests with
#
#   python3 -m unittest discover tests/ --failfast --quiet

import unittest
from PySide2 import QtCore, QtWidgets, QtGui, QtTest
from t_rex_typer import tab_safe_line_edit


if not QtWidgets.QApplication.instance():
    APP = QtWidgets.QApplication([])  # pragma: no cover


class TestTabSafeLineEdit(unittest.TestCase):

    def setUp(self):
        self.tab_safe = tab_safe_line_edit.TabSafeLineEdit()

    def test_is_a_line_edit(self):
        self.assertIsInstance(self.tab_safe, QtWidgets.QLineEdit)

    def test_retains_focus_when_tab_key_pressed(self):
        container    = QtWidgets.QWidget()
        other_widget = QtWidgets.QLineEdit()
        other_widget.setObjectName("other widget")
        layout       = QtWidgets.QVBoxLayout()
        layout.addWidget(self.tab_safe)
        layout.addWidget(other_widget)
        container.setLayout(layout)

        self.tab_safe.setFocus(QtCore.Qt.OtherFocusReason)

        self.assertEqual(container.focusWidget(), self.tab_safe)

        QtTest.QTest.keyPress(self.tab_safe, "\t")

        self.assertEqual(container.focusWidget(), self.tab_safe)

    def test_a_tab_is_inserted_when_tab_is_pressed(self):
        self.tab_safe.setText("text")
        self.assertEqual(self.tab_safe.text(), "text")

        QtTest.QTest.keyPress(self.tab_safe, "\t")

        self.assertEqual(self.tab_safe.text(), "text\t")
