# test_t_rex_typer.py
#
# Run tests with
#
#   python3 -m unittest discover tests/ --failfast --quiet
#
#   python3 -m unittest tests.test_t_rex_typer.TestClass.test_method

import os
import sys

from t_rex_typer import IS_DEV_DEBUG
# because AssertionError happens on False, not True
assert not IS_DEV_DEBUG, "Running in debug mode.  Disable to debug mode run tests."

import tempfile
import unittest
import unittest.mock
import warnings
import nostalgic

import shiboken2
from PySide2 import QtCore, QtWidgets, QtGui, QtTest

from tests.dialog_action import DialogAction

import t_rex_typer
from t_rex_typer import settings
from t_rex_typer import main_window
from t_rex_typer.text_label import TextLabel
from t_rex_typer.tab_safe_line_edit import TabSafeLineEdit

# silence the log
import logging
log = logging.getLogger("t_rex_typer")
log.setLevel(logging.ERROR)


if not QtWidgets.QApplication.instance():
    QtWidgets.QApplication([])  # pragma: no cover


class MainWindowCommon(unittest.TestCase):

    def setUp(self):
        # tests may change the value of IN_TESTING
        main_window.IN_TESTING = True
        self.test_filename     = None
        self.main_window       = main_window.MainWindow()

    def tearDown(self):
        if self.test_filename and os.path.isfile(self.test_filename):
            os.remove(self.test_filename)
            if os.path.exists(self.test_filename):
                warnings.warn(f"[WARNING]: Failed to remove: '{self.test_filename}'")  # pragma: no cover

            self.test_filename = None


class TestMainWindow(MainWindowCommon):

    def test_main_window_is_qmainwindow(self):
        self.assertIsInstance(self.main_window, QtWidgets.QMainWindow)

    def test_title_is_application_name(self):
        self.assertEqual(self.main_window.windowTitle(), t_rex_typer.APPLICATION_NAME)

    def test_uses_the_application_icon(self):
        main_window_icon = self.main_window.windowIcon()
        self.assertEqual(
            main_window_icon.cacheKey(),
            t_rex_typer.APPLICATION_ICON.cacheKey())

    def test_window_flags(self):
        # https://stackoverflow.com/a/40007740
        flags = self.main_window.windowFlags()
        self.assertTrue(int(flags & QtCore.Qt.WindowTitleHint))
        self.assertTrue(int(flags & QtCore.Qt.WindowMinMaxButtonsHint))
        self.assertTrue(int(flags & QtCore.Qt.WindowCloseButtonHint))
        self.assertTrue(int(flags & ~QtCore.Qt.WindowContextHelpButtonHint))

    def test_has_an_about_window(self):
        self.assertIsInstance(self.main_window.about_window, main_window.AboutWindow)

    def test_has_a_menu_bar(self):
        self.assertIsInstance(self.main_window.menu_bar, QtWidgets.QMenuBar)

    def test_has_a_file_menu(self):
        self.assertIsInstance(self.main_window.file_menu, QtWidgets.QMenu)
        self.assertEqual(self.main_window.file_menu.parent(), self.main_window.menu_bar)

    def test_has_an_open_action(self):
        self.assertIsInstance(self.main_window.open_action, QtWidgets.QAction)

    def test_has_an_save_action(self):
        self.assertIsInstance(self.main_window.save_action, QtWidgets.QAction)

    def test_has_an_save_as_action(self):
        self.assertIsInstance(self.main_window.save_as_action, QtWidgets.QAction)

    def test_has_load_dictionary_action(self):
        self.assertIsInstance(self.main_window.load_dictionary_action, QtWidgets.QAction)

    def test_has_an_exit_action(self):
        self.assertIsInstance(self.main_window.exit_action, QtWidgets.QAction)

    def test_has_a_settings_action(self):
        self.assertIsInstance(self.main_window.settings_action, QtWidgets.QAction)

    def test_has_a_help_menu(self):
        self.assertIsInstance(self.main_window.help_menu, QtWidgets.QMenu)
        self.assertEqual(self.main_window.help_menu.parent(), self.main_window.menu_bar)

    def test_has_a_text_label(self):
        self.assertIsInstance(self.main_window.text_label, TextLabel)

    def test_has_a_tab_safe_line_edit(self):
        self.assertIsInstance(self.main_window.line_edit, TabSafeLineEdit)

    def test_has_a_text_edit(self):
        self.assertIsInstance(self.main_window.text_edit, QtWidgets.QTextEdit)

    def test_has_a_restart_button(self):
        self.assertIsInstance(self.main_window.restart_button, QtWidgets.QPushButton)
        self.assertEqual(self.main_window.restart_button.text(), "Restart")

    def test_has__dictionary_attribute(self):
        self.assertTrue(hasattr(self.main_window, '_dictionary'))

    def test_has_lesson_file_attribute(self):
        self.assertTrue(hasattr(self.main_window, 'lesson_file'))

    def test_lesson_file_none_by_default(self):
        self.assertIsNone(self.main_window.lesson_file)

    def test_reset_clears_line_edit(self):
        self.main_window.line_edit.setText("test text")
        self.assertEqual(self.main_window.line_edit.text(), "test text")

        self.main_window._reset()

        self.assertEqual(self.main_window.line_edit.text(), "")

    def test_reset_sets__current_unit_to_first_word_of_text_edit(self):
        self.assertEqual(self.main_window.current_unit, '')

        self.main_window.text_edit.setText("reset sets current word")
        self.assertEqual(
            self.main_window.text_edit.toPlainText(),
            "reset sets current word")

        self.main_window._reset()

        self.assertEqual(self.main_window.current_unit, "reset")

    def test_reset_sets_text_label_to_split_live(self):
        self.assertEqual(self.main_window.text_label.toPlainText(), "")
        self.main_window.text_edit.setText(
            "test reset sets text label to split live")
        self.assertEqual(
            self.main_window.text_edit.toPlainText(),
            "test reset sets text label to split live")

        self.main_window._reset()

        self.assertEqual(
            self.main_window.text_label.toPlainText(),
            "test reset sets text label to split live")

    def test_reset_underlines_current_unit_in_text_label(self):
        cursor = self.main_window.text_label.textCursor()

        self.assertEqual(self.main_window.text_label.toPlainText(), "")
        self.main_window.split_raw = ("first", "unit", "should", "be", "underlined")

        cursor.setPosition(0)
        self.assertEqual(cursor.position(), 0)
        self.assertFalse(cursor.charFormat().fontUnderline())

        self.main_window._reset()

        cursor.setPosition(0)
        self.assertEqual(cursor.position(), 0)
        self.assertTrue(cursor.charFormat().fontUnderline())

        # ...and only the current unit
        p = len(self.main_window.current_unit)+ 1
        cursor.setPosition(p)
        self.assertEqual(cursor.position(), p)
        self.assertFalse(cursor.charFormat().fontUnderline())

    def test_reset_enables_line_edit_only_when_text_split(self):
        # try with empty split_raw
        self.assertFalse(self.main_window.line_edit.isEnabled())
        self.assertEqual(self.main_window.split_raw, ())

        self.main_window._reset()

        self.assertFalse(self.main_window.line_edit.isEnabled())

        # retry with split_raw assigned
        self.main_window.split_raw = ("some", "split", "text")
        self.assertEqual(self.main_window.split_raw, ("some", "split", "text"))

        self.main_window._reset()

        self.assertTrue(self.main_window.line_edit.isEnabled())

    def test_reset_sets_split_live_only_when_split_raw_exists(self):
        # try with empty split_raw
        self.assertEqual(self.main_window.split_raw, ())
        self.assertEqual(self.main_window.split_live, [])

        self.main_window._reset()

        self.assertEqual(self.main_window.split_live, [])

        # retry with split_raw assigned
        self.main_window.split_raw = ("some", "split", "text")
        self.assertEqual(self.main_window.split_raw, ("some", "split", "text"))

        self.main_window._reset()

        self.assertEqual(self.main_window.split_live, ["some", "split", "text"])

    def test_reset_sets_run_state_to_complete(self):
        self.main_window.run_state = t_rex_typer.main_window.RunState.PRACTICING
        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.PRACTICING)

        self.main_window._reset()

        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.READY)

    def test_reset_resets_missed(self):
        self.main_window.missed = 1
        self.assertEqual(self.main_window.missed, 1)

        self.main_window._reset()

        self.assertEqual(self.main_window.missed, 0)

    def test_reset_resets_last_time(self):
        self.main_window._last_time = 1660265287.6865938
        self.assertEqual(self.main_window._last_time, 1660265287.6865938)

        self.main_window._reset()

        self.assertEqual(self.main_window._last_time, 0)

    def test_save_file_resets_text_edit_document_status(self):
        self.test_filename = f"{tempfile.gettempdir()}/test_save_file_resets_text_edit_document_status.txt"
        self.assertFalse(self.main_window.text_edit.document().isModified())
        QtTest.QTest.keyClicks(
            self.main_window.text_edit,
            "test_save_file_resets_text_edit_document_status")
        self.assertTrue(self.main_window.text_edit.document().isModified())

        self.main_window._save_file(self.test_filename)

        self.assertFalse(self.main_window.text_edit.document().isModified())

    def test_has_text_raw_attribute(self):
        self.assertEqual(self.main_window.text_raw, '')

    def test_has_split_raw_attribute(self):
        self.assertEqual(self.main_window.split_raw, ())

    def test_has_split_live_attribute(self):
        self.assertEqual(self.main_window.split_live, [])

    def test_has_current_unit_attribute(self):
        self.assertTrue(hasattr(self.main_window, 'current_unit'))

    def test_initial_current_unit(self):
        self.assertEqual(self.main_window.current_unit, '')

    def test_set_window_title_method_appends_argument_to_application_name_in_window_title(self):
        self.assertEqual(
            self.main_window.windowTitle(),
            t_rex_typer.APPLICATION_NAME)

        self.main_window.set_window_title("test set_window_title")

        self.assertEqual(
            self.main_window.windowTitle(),
            f"{t_rex_typer.APPLICATION_NAME} - test set_window_title")

    def test_set_window_title_method_sets_window_title_to_application_name_when_passed_nothing(self):
        self.main_window.setWindowTitle(f"{t_rex_typer.APPLICATION_NAME} - test set_window_title")

        self.assertEqual(
            self.main_window.windowTitle(),
            f"{t_rex_typer.APPLICATION_NAME} - test set_window_title")

        self.main_window.set_window_title()

        self.assertEqual(
            self.main_window.windowTitle(),
            t_rex_typer.APPLICATION_NAME)

    def test_has_run_state_property(self):
        self.assertTrue(hasattr(self.main_window, 'run_state'))

    def test_initial_run_state(self):
        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.READY)

    def test_has_missed_attribute(self):
        self.assertTrue(hasattr(self.main_window, 'missed'))

    def test_initial_missed_count(self):
        self.assertEqual(self.main_window.missed, 0)

    def test_has_last_time_attribute(self):
        self.assertTrue(hasattr(self.main_window, '_last_time'))

    def test_initial_last_time(self):
        # team of last content entered
        self.assertEqual(self.main_window._last_time, 0)

    def test_has_is_maybe_miss_attribute(self):
        self.assertTrue(hasattr(self.main_window, '_is_maybe_miss'))

    def test_initial_is_maybe_miss(self):
        self.assertEqual(self.main_window._is_maybe_miss, False)

    def test_has_is_miss_attribute(self):
        self.assertTrue(hasattr(self.main_window, '_is_miss'))

    def test_initial_is_miss(self):
        self.assertEqual(self.main_window._is_miss, False)



    def test_close_also_closes_about_window(self):
        mock_close = unittest.mock.MagicMock()
        self.main_window.about_window.close = mock_close

        self.main_window.close()

        mock_close.assert_called_once()

    def test_close_deletes_about_window(self):
        self.assertTrue(shiboken2.isValid(self.main_window.about_window))
        self.main_window.close()
        self.assertFalse(hasattr(self.main_window, "about_window"))

    def test_close_also_closes_settings_window(self):
        mock_close = unittest.mock.MagicMock()
        self.main_window.settings_window.close = mock_close

        self.main_window.close()

        mock_close.assert_called_once()

    def test_close_deletes_settings_window(self):
        self.assertTrue(shiboken2.isValid(self.main_window.settings_window))
        self.main_window.close()
        self.assertFalse(hasattr(self.main_window, "settings_window"))


class SettingsCommon(unittest.TestCase):

    def setUp(self):
        # tests may change the value of IN_TESTING
        main_window.IN_TESTING = True

        self.test_settings_file = tempfile.NamedTemporaryFile(
            dir=f"{tempfile.gettempdir()}",
            prefix=f"{__name__}_",
            suffix=".ini",
            # delete=False,  # uncomment to inspect; prefixes "test." when False
        ).name

        # create a new settings instance
        self.start_new_application()

    def start_new_application(self):
        nostalgic.Configuration._SingletonMetaclass__reset()

        # must redefine settings within each module since it's already
        # been imported.  Note that we reuse the original temp
        # config_file.
        t_rex_typer.SETTINGS             = nostalgic.Configuration(self.test_settings_file)
        t_rex_typer.main_window.SETTINGS = t_rex_typer.SETTINGS
        t_rex_typer.settings.SETTINGS    = t_rex_typer.SETTINGS

        self.main_window = main_window.MainWindow()
        assert t_rex_typer.SETTINGS.config_file[:4]  == '/tmp', "Not using test settings"

    def close_and_kill_application(self):
        self.main_window.close()

        if shiboken2.isValid(self.main_window):
            shiboken2.delete(self.main_window)

        self.assertFalse(shiboken2.isValid(self.main_window))
        del self.main_window

        self.assertFalse(hasattr(self, 'main_window'))

    def tearDown(self):
        if os.path.isfile(self.test_settings_file):
            os.remove(self.test_settings_file)
            if os.path.exists(self.test_settings_file):
                warnings.warn(f"[WARNING]: Failed to remove: '{self.test_settings_file}'")  # pragma: no cover


class TestSettings(SettingsCommon):

    def test_config_file_name(self):
        self.assertEqual(
            t_rex_typer.SETTING_FILE_NAME,
            f".config/{t_rex_typer.APPLICATION_NAME}")

    def test_config_path(self):
        if sys.platform == "linux":
            # ~/.config/T-Rex Typer/T-Rex Typer.ini
            expected_settings_path = os.path.join(
                os.path.expanduser("~"),
                f".config/{t_rex_typer.APPLICATION_NAME}")

            self.assertEqual(t_rex_typer.SETTINGS_PATH, expected_settings_path)

    def test_main_window_initializes_settings(self):
        mock_init = unittest.mock.MagicMock()
        original_init = settings.init
        settings.init = mock_init

        self.close_and_kill_application()
        self.start_new_application()

        mock_init.assert_called_once()

        # must replace since this is a module level function which is
        # used elsewhere
        settings.init = original_init

    def test_main_window_loads_settings(self):
        mock_load = unittest.mock.MagicMock()
        original_load = settings.load
        settings.load = mock_load

        self.close_and_kill_application()
        self.start_new_application()

        mock_load.assert_called_once()

        # must replace since this is a module level function which is
        # used elsewhere
        settings.load = original_load

    def test_create_settings_on_main_window_first_close(self):
        # NamedTemporaryFile "is guaranteed to have a visible name in
        # the file system".  When delete=False for test development,
        # the file is created immediately causing the first assert to
        # fail.  The check and delete prevents a Catch-22–can't look
        # at temp file if delete=True; can't run tests if
        # delete=False.
        if os.path.exists(self.test_settings_file):
            os.remove(self.test_settings_file)
        self.assertFalse(os.path.exists(self.test_settings_file))

        self.close_and_kill_application()

        self.assertTrue(os.path.exists(self.test_settings_file))

    def test_has_main_window_geometry(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "main_window_geometry"))

    def test_save_and_restore_main_window_geometry(self):
        new_geometry = QtCore.QRect(0, 0, 100, 100)
        self.assertNotEqual(self.main_window.geometry(), new_geometry)
        self.main_window.setGeometry(new_geometry)
        self.assertEqual(self.main_window.geometry(), new_geometry)

        self.close_and_kill_application()
        self.start_new_application()

        self.assertEqual(self.main_window.geometry(), new_geometry)

    def test_has_main_window_size(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "main_window_size"))

    def test_save_and_restore_main_window_size(self):
        # NOTE: somewhat redundant, as size is also restored by
        # geometry.  Kill main_window_geometry in order to force the
        # use of size.
        has_geometry_setting =  lambda : 'main_window_geometry' in t_rex_typer.SETTINGS.__dict__['_settings']
        self.assertTrue(has_geometry_setting())
        t_rex_typer.SETTINGS.__dict__['_settings'].pop('main_window_geometry')
        self.assertFalse(has_geometry_setting())

        new_size = (100, 100)
        self.assertNotEqual(self.main_window.size().toTuple(), new_size)
        self.main_window.resize(*new_size)
        self.assertEqual(self.main_window.size().toTuple(), new_size)

        self.close_and_kill_application()
        self.start_new_application()

        self.assertEqual(self.main_window.size().toTuple(), new_size)

    def test_has_main_window_pos(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "main_window_pos"))

    def test_save_and_restore_main_window_pos(self):
        new_pos = QtCore.QPoint(0, 0)
        self.assertNotEqual(self.main_window.pos(), new_pos)
        self.main_window.move(new_pos)
        self.assertEqual(self.main_window.pos(), new_pos)

        self.close_and_kill_application()
        self.start_new_application()

        self.assertEqual(self.main_window.pos(), QtCore.QPoint(0, 0))

    def test_has_main_window_maximized(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "main_window_maximized"))

    def test_save_and_restore_main_window_maximized(self):
        self.assertFalse(self.main_window.isMaximized())
        self.main_window.showMaximized()
        self.assertTrue(self.main_window.isMaximized())

        self.close_and_kill_application()
        self.start_new_application()

        self.assertTrue(self.main_window.isMaximized())

    def test_has_main_window_splitter_h_state(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "main_window_splitter_h_state"))

    def test_save_and_restore_main_window_splitter_h_state(self):
        starting_state = self.main_window.splitter_h.saveState()
        self.main_window.splitter_h.moveSplitter(0, 0)
        new_state = self.main_window.splitter_h.saveState()
        self.assertNotEqual(new_state, starting_state)

        self.close_and_kill_application()
        self.start_new_application()

        self.assertEqual(self.main_window.splitter_h.saveState(), new_state)

    def test_has_main_window_splitter_v_state(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "main_window_splitter_v_state"))

    def test_save_and_restore_main_window_splitter_v_state(self):
        starting_state = self.main_window.splitter_v.saveState()
        self.main_window.splitter_v.moveSplitter(0, 0)
        new_state = self.main_window.splitter_v.saveState()
        self.assertNotEqual(new_state, starting_state)

        self.close_and_kill_application()
        self.start_new_application()

        self.assertEqual(self.main_window.splitter_v.saveState(), new_state)

    def test_has_wpm_threshold(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "wpm_threshold"))

    def test_default_wpm_threshold(self):
        self.assertEqual(t_rex_typer.SETTINGS['wpm_threshold']._default, 30)

    def test_save_and_restore_wpm_threshold(self):
        starting_value = self.main_window.settings_window.wpm_threshold_spinbox.value()
        new_value = 42
        self.assertNotEqual(starting_value, new_value)

        self.main_window.settings_window.wpm_threshold_spinbox.setValue(new_value)
        self.assertEqual(
            self.main_window.settings_window.wpm_threshold_spinbox.value(),
            new_value)

        self.close_and_kill_application()
        self.start_new_application()

        self.assertEqual(
            self.main_window.settings_window.wpm_threshold_spinbox.value(),
            new_value)

    def test_has_lesson_directory(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "lesson_directory"))

    def test_default_lesson_directory(self):
        self.assertEqual(
            t_rex_typer.SETTINGS['lesson_directory']._default,
            os.path.expanduser("~"))

    def test_save_and_restore_lesson_directory(self):
        starting_value = self.main_window.settings_window.lesson_directory_line_edit.text()
        new_value = "/path/to/lesson/directory"
        self.assertNotEqual(starting_value, new_value)

        self.main_window.settings_window.lesson_directory_line_edit.setText(new_value)
        self.assertEqual(
            self.main_window.settings_window.lesson_directory_line_edit.text(),
            new_value)

        self.close_and_kill_application()
        self.start_new_application()

        self.assertEqual(
            self.main_window.settings_window.lesson_directory_line_edit.text(),
            new_value)

    def test_has_lesson_director(self):
        self.assertTrue(hasattr(t_rex_typer.SETTINGS, "dictionary_directory"))

    def test_default_dictionary_directory(self):
        self.assertEqual(
            t_rex_typer.SETTINGS['dictionary_directory']._default,
            os.path.expanduser("~"))

    def test_save_and_restore_dictionary_directory(self):
        starting_value = self.main_window.settings_window.dictionary_directory_line_edit.text()
        new_value = "/path/to/dictionary/directory"
        self.assertNotEqual(starting_value, new_value)

        self.main_window.settings_window.dictionary_directory_line_edit.setText(new_value)
        self.assertEqual(
            self.main_window.settings_window.dictionary_directory_line_edit.text(),
            new_value)

        self.close_and_kill_application()
        self.start_new_application()

        self.assertEqual(
            self.main_window.settings_window.dictionary_directory_line_edit.text(),
            new_value)


class TestMainWindowLineEdit(MainWindowCommon):

    def enable_line_edit(self, run_state=None):
        self.main_window.line_edit.setEnabled(True)
        self.assertTrue(self.main_window.line_edit.isEnabled())
        if run_state:
            self.main_window.run_state = run_state
            self.assertEqual(self.main_window.run_state, run_state)

    def test_disabled_on_start(self):
        self.assertFalse(self.main_window.line_edit.isEnabled())

    def test_text_edited_signal_calls_slot(self):
        self.main_window.on_line_edit_text_edited = unittest.mock.MagicMock()
        self.main_window.line_edit.textEdited.emit("test text_edited slot")
        self.main_window.on_line_edit_text_edited.assert_called()


class TestPractice(MainWindowCommon):

    def enable_line_edit(self, run_state=None):
        self.main_window.line_edit.setEnabled(True)
        self.assertTrue(self.main_window.line_edit.isEnabled())
        if run_state:
            self.main_window.run_state = run_state
            self.assertEqual(self.main_window.run_state, run_state)

    def test_text_edited_sets_run_state_to_practicing(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "entering text changes state to PRACTICING")

        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.PRACTICING)

    def test_run_state_stays_complete_when_text_edited(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.COMPLETE)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "entering text changes nothing")

        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.COMPLETE)

    def test_text_label_shows_statistics_on_complete(self):
        # 100%
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_raw    = ("show", "stats", "on",  "complete", )
        self.main_window.split_live   = ["show", "stats", "on",  "complete", ]
        self.main_window.current_unit = "show"

        QtTest.QTest.keyClicks(self.main_window.line_edit, "show stats on complete")

        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.COMPLETE)
        self.assertEqual(
            self.main_window.text_label.toPlainText(),
            "CONGRATS! Missed: 0 Accuracy: 100%")

        # 75%
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_raw  = ("show", "stats", "on",  "complete", )
        self.main_window.split_live = ["show", "stats", "on",  "complete", ]
        self.main_window.current_unit = "show"

        # make mistake
        QtTest.QTest.keyClicks(
            self.main_window.line_edit,
            "h",
            QtCore.Qt.KeyboardModifier.NoModifier,
            1  # ms
        )

        # correct it
        QtTest.QTest.keyClicks(self.main_window.line_edit, "\b")
        QtTest.QTest.keyClicks(self.main_window.line_edit, "\bshow stats on complete")

        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.COMPLETE)
        self.assertEqual(
            self.main_window.text_label.toPlainText(),
            "CONGRATS! Missed: 1 Accuracy: 75%")

        # 50%
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_raw  = ("show", "stats", "on",  "complete", )
        self.main_window.split_live = ["show", "stats", "on",  "complete", ]
        self.main_window.current_unit = "show"

        # make mistake
        QtTest.QTest.keyClicks(
            self.main_window.line_edit,
            "h",
            QtCore.Qt.KeyboardModifier.NoModifier,
            1  # ms
        )

        # correct it
        QtTest.QTest.keyClicks(self.main_window.line_edit, "\b")
        QtTest.QTest.keyClicks(self.main_window.line_edit, "show")

        # make another mistake
        QtTest.QTest.keyClicks(self.main_window.line_edit, "t")
        QtTest.QTest.keyClicks(self.main_window.line_edit, "\b")
        QtTest.QTest.keyClicks(self.main_window.line_edit, "stats on complete")

        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.COMPLETE)
        self.assertEqual(
            self.main_window.text_label.toPlainText(),
            "CONGRATS! Missed: 2 Accuracy: 50%")


    def test_first_element_from_split_live_removed_when_content_matches_current_unit(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_live = ["my", "text", "split", ]
        self.main_window.current_unit = "my"

        # insert contents
        QtTest.QTest.keyClicks(self.main_window.line_edit, "my")

        self.assertEqual(self.main_window.split_live, ["text", "split"])

    def test_current_unit_becomes_next_split_live_element_when_content_matches_current_unit(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_live = ["next", "split", ]
        self.main_window.current_unit = "next"

        # insert contents
        QtTest.QTest.keyClicks(self.main_window.line_edit, "next")

        self.assertEqual(self.main_window.current_unit, "split")

    def test_next_units_black_when_advancing(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)

        self.assertEqual(self.main_window.text_label.toPlainText(), "")
        self.main_window.split_raw = ("next", "units", "black", "after", "advancing", )
        self.main_window._reset()

        cursor = self.main_window.text_label.textCursor()

        # confirm they're black
        for i in range(len(self.main_window.text_label.toPlainText())):
            cursor.setPosition(i)
            self.assertEqual(cursor.position(), i)
            self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.BLACK)

        # insert contents
        QtTest.QTest.keyClicks(self.main_window.line_edit, self.main_window.split_raw[0])

        # confirm they're still black
        for i in range(len(self.main_window.text_label.toPlainText())):
            cursor.setPosition(i)
            self.assertEqual(cursor.position(), i)
            self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.BLACK)

    def test_current_unit_underlined_maintained_when_advancing(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)

        self.assertEqual(self.main_window.text_label.toPlainText(), "")
        self.main_window.split_raw = ("stay", "underlined", "after", "advancing", )
        self.main_window._reset()

        cursor = self.main_window.text_label.textCursor()

        cursor.setPosition(0)
        self.assertEqual(cursor.position(), 0)
        self.assertTrue(cursor.charFormat().fontUnderline())

        # ...and only the current unit
        p = len(self.main_window.current_unit)+ 1
        cursor.setPosition(p)
        self.assertEqual(cursor.position(), p)
        self.assertFalse(cursor.charFormat().fontUnderline())

        # insert contents
        QtTest.QTest.keyClicks(self.main_window.line_edit, "stay")

        cursor.setPosition(0)
        self.assertEqual(cursor.position(), 0)
        self.assertEqual(self.main_window.text_label.toPlainText(), "underlined after advancing")
        self.assertTrue(cursor.charFormat().fontUnderline())

        # ...and only the current unit
        p = len(self.main_window.current_unit)+ 1
        cursor.setPosition(p)
        self.assertEqual(cursor.position(), p)
        self.assertFalse(cursor.charFormat().fontUnderline())

    def test_run_state_becomes_complete_when_contents_match_final_element_of_split_live(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_live = ["complete", ]
        self.main_window.current_unit = "complete"

        # insert contents
        QtTest.QTest.keyClicks(self.main_window.line_edit, "complete")

        self.assertEqual(self.main_window.run_state, t_rex_typer.main_window.RunState.COMPLETE)

    def test_line_edit_disabled_when_contents_match_final_element_of_split_live(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_live = ["disable", ]
        self.main_window.current_unit = "disable"

        # insert contents
        QtTest.QTest.keyClicks(self.main_window.line_edit, "disable")

        self.assertFalse(self.main_window.line_edit.isEnabled())

    def test_line_edit_clears_when_content_matches(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_live = ["line", "edit", "should", "clear", ]
        self.main_window.current_unit = "line"

        # insert contents that doesn't quite match
        QtTest.QTest.keyClicks(self.main_window.line_edit, "lin")

        self.assertEqual(self.main_window.line_edit.text(), "lin")

        # match contents
        QtTest.QTest.keyClicks(self.main_window.line_edit, "e")

        self.assertEqual(self.main_window.line_edit.text(), "")

    def test_text_label_removes_first_word_when_content_matches(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_live = ["text", "label", "should", "not", "have", "first", "word", "anymore",]
        self.main_window.current_unit = "text"
        self.main_window.text_label.setText("text")
        self.assertEqual(self.main_window.text_label.toPlainText(), "text")

        QtTest.QTest.keyClicks(self.main_window.line_edit, "text")

        self.assertEqual(
            self.main_window.text_label.toPlainText(),
            "label should not have first word anymore")

    def test_text_label_matched_characters_get_grayed_out(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_live = ["gray", ]
        self.main_window.current_unit = "gray"
        self.main_window.text_label.setText("gray")
        self.assertEqual(self.main_window.text_label.toPlainText(), "gray")

        cursor = self.main_window.text_label.textCursor()

        for i in range(3):
            cursor.setPosition(i)
            self.assertEqual(cursor.position(), i)
            self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.BLACK)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "grey")

        for i in [0, 1, 2, 4]:
            cursor.setPosition(i)
            self.assertEqual(cursor.position(), i)
            self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.GRAY)

        cursor.setPosition(3)
        self.assertEqual(cursor.position(), 3)
        self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.BLACK)

    def test_first_character_gets_recolored_after_deleting_word_with_correct_first_character(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.split_live = ["all", "bad", ]
        self.main_window.current_unit = "all"
        self.main_window.text_label.setText("all")
        self.assertEqual(self.main_window.text_label.toPlainText(), "all")

        cursor = self.main_window.text_label.textCursor()

        for i in range(3):
            cursor.setPosition(i)
            self.assertEqual(cursor.position(), i)
            self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.BLACK)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "ate")

        cursor.setPosition(0)
        self.assertEqual(cursor.position(), 0)
        self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.GRAY)

        cursor.setPosition(1)
        self.assertEqual(cursor.position(), 1)
        self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.GRAY)

        cursor.setPosition(2)
        self.assertEqual(cursor.position(), 2)
        self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.BLACK)

        cursor.setPosition(3)
        self.assertEqual(cursor.position(), 3)
        self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.BLACK)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "\b\b\b")

        for i in range(3):
            cursor.setPosition(i)
            self.assertEqual(cursor.position(), i)
            self.assertEqual(cursor.charFormat().foreground().color(), t_rex_typer.BLACK)

    def test_is_maybe_miss_when_not_miss_and_content_longer_than_current_unit(self):
        # first stroke inserts word longer than the final word

        # HREBGS/K-L > election > lexical
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.current_unit = "lexical"
        self.assertFalse(self.main_window._is_miss)
        self.assertFalse(self.main_window._is_maybe_miss)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "election")

        self.assertTrue(self.main_window._is_maybe_miss)

    def test_no_content_is_maybe_a_miss(self):
        # already started and then returned to position 0
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.main_window.current_unit = "dummy"
        self.assertFalse(self.main_window._is_miss)
        self.assertFalse(self.main_window._is_maybe_miss)

        test_word = "test"

        QtTest.QTest.keyClicks(self.main_window.line_edit, test_word)

        self.assertEqual(self.main_window.line_edit.text(), test_word)

        backspaces = ''.join(["\b" for i in range(len("test"))])
        QtTest.QTest.keyClicks(self.main_window.line_edit, backspaces)

        self.assertEqual(self.main_window.line_edit.text(), "")

        self.assertTrue(self.main_window._is_maybe_miss)

    def test_matching_current_unit_resets_maybe_miss(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        # make sure we don't complete
        self.main_window.split_live = ["match", "current", ]
        self.main_window.current_unit = "match"
        self.main_window._is_maybe_miss = True
        self.assertTrue(self.main_window._is_maybe_miss)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "match")

        self.assertFalse(self.main_window._is_maybe_miss)

    def test_maybe_a_miss_if_contents_have_non_matching_characters(self):
        # might be two-stroke word where the first stroke doesn't
        # match; or it could just be wrong
        #
        # Example:
        #
        #   HREBG/TOR > lecture > elector
        #
        # NOTE: 'lecture' and 'elector' have the same length.  This is
        # different than with 'election' and 'lexical' (HREBGS/K-L >
        # election > lexical) where it's the length that makes us
        # suspicious.
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        # make sure we don't complete
        self.main_window.split_live = ["lecture", "test", ]
        self.main_window.current_unit = "elector"
        self.main_window._is_maybe_miss = False
        self.assertFalse(self.main_window._is_maybe_miss)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "lecture")

        self.assertTrue(self.main_window._is_maybe_miss)

    def test_matched_word_resets_is_miss(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        # make sure we don't complete
        self.main_window.split_live = ["was", "miss", ]
        self.main_window.current_unit = "was"
        self.main_window._is_miss = True
        self.assertTrue(self.main_window._is_miss)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "was")

        self.assertFalse(self.main_window._is_miss)

    def test_miss_if_maybe_a_miss_and_time_between_strokes_is_too_slow(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        # make sure we don't complete
        self.main_window.split_live = ["will", "miss", ]
        self.main_window.current_unit = "will"
        self.main_window._is_maybe_miss = True
        self.assertTrue(self.main_window._is_maybe_miss)
        self.main_window._is_miss = False
        self.assertFalse(self.main_window._is_miss)

        QtTest.QTest.keyClicks(
            self.main_window.line_edit,
            "was",
            QtCore.Qt.KeyboardModifier.NoModifier,
            5  # ms
        )

        self.assertTrue(self.main_window._is_miss)

    def test_miss_increments_miss_count(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        # make sure we don't complete
        self.main_window.split_live = ["do", "miss", ]
        self.main_window.current_unit = "do"
        self.main_window._is_maybe_miss = True
        self.assertTrue(self.main_window._is_maybe_miss)
        self.main_window._is_miss = False
        self.assertFalse(self.main_window._is_miss)
        self.main_window.missed = 0
        self.assertEqual(self.main_window.missed, 0)

        QtTest.QTest.keyClicks(
            self.main_window.line_edit,
            "was",
            QtCore.Qt.KeyboardModifier.NoModifier,
            5  # ms
        )

        self.assertEqual(self.main_window.missed, 1)

    def test_dont_double_count_misses(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        # make sure we don't complete
        self.main_window.split_live = ["already", "missed", ]
        self.main_window.current_unit = "already"
        self.main_window._is_maybe_miss = True
        self.assertTrue(self.main_window._is_maybe_miss)
        self.main_window._is_miss = True
        self.assertTrue(self.main_window._is_miss)
        self.main_window.missed = 1
        self.assertEqual(self.main_window.missed, 1)

        QtTest.QTest.keyClicks(
            self.main_window.line_edit,
            "was",
            QtCore.Qt.KeyboardModifier.NoModifier,
            5  # ms
        )

        self.assertEqual(self.main_window.missed, 1)

    def test_new_content_updates_last_time(self):
        self.enable_line_edit(run_state=t_rex_typer.main_window.RunState.READY)
        self.assertEqual(self.main_window._last_time, 0)

        QtTest.QTest.keyClicks(self.main_window.line_edit, "stuff")

        self.assertGreater(self.main_window._last_time, 0)



class TestMainWindowTextEdit(MainWindowCommon):

    def test_text_edit_starts_with_placeholder_text(self):
        self.assertEqual(self.main_window.text_edit.placeholderText(), 'Put practice words here...')

    def test_text_edit_changed_enables_save_as_action(self):
        self.assertEqual(self.main_window.text_edit.toPlainText(), '')
        self.assertFalse(self.main_window.save_as_action.isEnabled())

        QtTest.QTest.keyClicks(self.main_window.text_edit, "changing the text edit")

        self.assertTrue(self.main_window.save_as_action.isEnabled())

    def test_change_adds_star_to_application_name_in_window_title_when_no_lesson_file(self):
        self.assertEqual(self.main_window.text_edit.toPlainText(), '')
        self.assertEqual(self.main_window.windowTitle(), t_rex_typer.APPLICATION_NAME)
        QtTest.QTest.keyClicks(self.main_window.text_edit, "should add a star")
        self.assertEqual(self.main_window.windowTitle(), t_rex_typer.APPLICATION_NAME + ' - *')

    def test_change_adds_star_to_lesson_name_in_window_title_when_lesson_is_loaded(self):
        self.main_window.lesson_file = "test_text_edit_appends_star_to_lesson_file_name.txt"
        self.main_window.set_window_title(self.main_window.lesson_file)

        self.assertEqual(
            self.main_window.windowTitle(),
            f"{t_rex_typer.APPLICATION_NAME} - {self.main_window.lesson_file}")

        self.assertEqual(self.main_window.text_edit.toPlainText(), '')
        QtTest.QTest.keyClicks(self.main_window.text_edit, "should append a star")
        self.assertEqual(
            self.main_window.windowTitle(),
            f"{t_rex_typer.APPLICATION_NAME} - {self.main_window.lesson_file}*")

    def test_change_sets_text_raw(self):
        self.assertEqual(self.main_window.text_raw, "")
        QtTest.QTest.keyClicks(self.main_window.text_edit, "should set text_raw")
        self.assertEqual(self.main_window.text_raw, "should set text_raw")

    def test_change_sets_split_text(self):
        self.assertEqual(self.main_window.split_raw, ())
        QtTest.QTest.keyClicks(self.main_window.text_edit, "should set split_raw")
        self.assertEqual(self.main_window.split_raw, ("should", "set", "split_raw"))

    def test_changed_signal_calls_slot(self):
        self.main_window._reset = unittest.mock.MagicMock()
        QtTest.QTest.keyClicks(self.main_window.text_edit, "should induce reset")
        self.main_window._reset.assert_called()


# https://stackoverflow.com/a/73001702
class TestApplicationStartsWithTextEditInFocus(MainWindowCommon):

    # called before the body of
    # test_application_starts_with_text_edit_in_focus is executed
    def setUp(self):
        super().setUp()

        # override the focus event handler
        def focusInEvent(event):
            QtWidgets.QApplication.instance().exit(0)  # Success

        text_edit = self.main_window.text_edit
        text_edit.focusInEvent = focusInEvent

        class KillApplicationIfTextEditNeverTakesFocus(QtCore.QObject):

            def eventFilter(self, obj, event):
                if event.type() == QtCore.QEvent.User:
                    QtWidgets.QApplication.instance().exit(1)  # Failure
                    return True
                else:
                    return QtCore.QObject.eventFilter(self, obj, event)

        kill_filter = KillApplicationIfTextEditNeverTakesFocus()
        self.main_window.installEventFilter(kill_filter)

        self.main_window.show()
        QtWidgets.QApplication.instance().postEvent(self.main_window, QtCore.QEvent(QtCore.QEvent.User))

        self.rv = QtWidgets.QApplication.instance().exec_()

    def test_application_starts_with_text_edit_in_focus(self):
        self.assertEqual(self.rv, 0)


class TestMainWindowRestartButton(MainWindowCommon):

    def test_restart_button_pressed_signal_calls_slot(self):
        self.main_window._reset = unittest.mock.MagicMock()
        QtTest.QTest.mouseClick(self.main_window.restart_button, QtCore.Qt.LeftButton)
        self.main_window._reset.assert_called_once()


class TestMainWindowFileMenu(MainWindowCommon):

    def test_file_menu_tool_tips_visible(self):
        self.assertTrue(self.main_window.file_menu.toolTipsVisible())

    def test_file_menu_has_open_action(self):
        self.assertIn(self.main_window.open_action, self.main_window.file_menu.actions())

    def test_file_menu_has_save_action(self):
        self.assertIn(self.main_window.save_action, self.main_window.file_menu.actions())

    def test_file_menu_has_save_as_action(self):
        self.assertIn(self.main_window.save_as_action, self.main_window.file_menu.actions())

    def test_file_menu_has_load_dictionary_action(self):
        self.assertIn(self.main_window.load_dictionary_action, self.main_window.file_menu.actions())

    def test_file_menu_has_setting_action(self):
        self.assertIn(self.main_window.settings_action, self.main_window.file_menu.actions())

    def test_file_menu_has_exit_action(self):
        self.assertIn(self.main_window.exit_action, self.main_window.file_menu.actions())


class TestMainWindowHelpMenu(MainWindowCommon):

    def test_tool_tips_visible(self):
        self.assertTrue(self.main_window.help_menu.toolTipsVisible())

    def test_has_about_action(self):
        self.assertIn(
            self.main_window.about_action,
            self.main_window.help_menu.actions())

    def test_about_trigger_shows_the_about_window(self):
        about_window_show_mock                       = unittest.mock.MagicMock()
        about_window_raise_mock                      = unittest.mock.MagicMock()
        about_window_activate_window_mock            = unittest.mock.MagicMock()

        self.main_window.about_window.show           = about_window_show_mock
        self.main_window.about_window.raise_         = about_window_raise_mock
        self.main_window.about_window.activateWindow = about_window_activate_window_mock

        self.main_window.about_action.triggered.emit()

        about_window_show_mock.assert_called_once()
        about_window_raise_mock.assert_called_once()
        about_window_activate_window_mock.assert_called_once()


class TestMainWindowSaveAction(MainWindowCommon):
    def setUp(self):
        super().setUp()
        self.test_filename = f"{tempfile.gettempdir()}/test_on_save.txt"

    def test_save_action_has_shortcut(self):
        self.assertGreater(len(self.main_window.save_action.shortcuts()), 0)

    def test_save_action_has_tooltip(self):
        self.assertIsNotNone(self.main_window.save_action.toolTip())

    def test_save_action_disabled_on_start(self):
        self.assertFalse(self.main_window.save_action.isEnabled())

    def test_save_error_creates_message_box(self):

        def induce_error(*arg, **kwargs):
            raise AssertionError("Test induced error")

        built_in_open = open
        main_window.open = induce_error

        # error is handled
        self.main_window._save_file(self.test_filename)

        # assumes that the message box gets shown
        self.assertIsInstance(self.main_window.message_box, QtWidgets.QMessageBox)
        self.assertEqual(self.main_window.message_box.windowTitle(), "T-Rex Terror")
        self.assertEqual(self.main_window.message_box.icon(), QtWidgets.QMessageBox.Warning)
        self.assertEqual(self.main_window.message_box.text(), f"Could not save file.")
        self.assertEqual(self.main_window.message_box.informativeText(), "Address the error below and try again.")
        self.assertNotEqual(self.main_window.message_box.detailedText(), "")

        main_window.open = built_in_open

    def test_save_trigger_writes_text_edit_text_to_lesson_file(self):
        text = "testing save as functionality"

        # confirm text edit contains text
        self.main_window.text_edit.setText(text)
        self.assertEqual(self.main_window.text_edit.toPlainText(), text)

        self.main_window.lesson_file = self.test_filename

        assert os.path.isfile(self.test_filename) == False, f"Test file was not cleaned up.  Delete '{self.test_filename}' and rerun tests."

        self.main_window.save_action.triggered.emit()

        self.assertTrue(os.path.isfile(self.test_filename))

    def test_save_action_enabled_when_lesson_file_set(self):
        self.assertFalse(self.main_window.save_action.isEnabled())
        self.main_window.lesson_file = "test save action enabled on lesson file set.txt"
        self.assertTrue(self.main_window.save_action.isEnabled())

    def test_save_action_disabled_when_lesson_file_not_set(self):
        self.main_window.lesson_file = "test save action disabled when lesson file not set.txt"
        self.assertTrue(self.main_window.save_action.isEnabled())

        self.main_window.lesson_file = ""
        self.assertFalse(self.main_window.save_action.isEnabled())


class TestMainWindowSaveAsAction(MainWindowCommon):

    def setUp(self):
        super().setUp()
        self.test_filename = f"{tempfile.gettempdir()}/test_on_save_as.txt"

    def test_save_as_action_has_tooltip(self):
        self.assertIsNotNone(self.main_window.save_as_action.toolTip())

    def test_save_as_action_disabled_on_start(self):
        self.assertFalse(self.main_window.save_as_action.isEnabled())

    def test_save_as_trigger_creates_a_file_on_selected(self):
        main_window.IN_TESTING = DialogAction.Selected

        text = "testing save as functionality"

        # confirm text edit contains text
        self.main_window.text_edit.setText(text)
        self.assertEqual(self.main_window.text_edit.toPlainText(), text)

        assert os.path.isfile(self.test_filename) == False, f"Test file was not cleaned up.  Delete '{self.test_filename}' and rerun tests."

        self.main_window.save_as_action.triggered.emit()

        self.assertTrue(os.path.isfile(self.test_filename))

    def test_save_as_trigger_cancel_does_not_create_a_file(self):
        main_window.IN_TESTING = DialogAction.Canceled

        text = "testing save as functionality"

        # confirm text edit contains text
        self.main_window.text_edit.setText(text)
        self.assertEqual(self.main_window.text_edit.toPlainText(), text)

        assert os.path.isfile(self.test_filename) == False, f"Test file was not cleaned up.  Delete '{self.test_filename}' and rerun tests."

        self.main_window.save_as_action.triggered.emit()

        self.assertFalse(os.path.isfile(self.test_filename))

    def test_save_as_trigger_sets_lesson_file_on_selected(self):
        main_window.IN_TESTING = DialogAction.Selected

        text = "testing save as functionality"

        # confirm text edit contains text
        self.main_window.text_edit.setText(text)
        self.assertEqual(self.main_window.text_edit.toPlainText(), text)

        self.assertIsNone(self.main_window.lesson_file)
        self.main_window.save_as_action.triggered.emit()

        self.assertEqual(self.main_window.lesson_file, self.test_filename)

    def test_save_as_trigger_doesnt_set_lesson_file_on_cancel(self):
        main_window.IN_TESTING = DialogAction.Canceled

        text = "testing save as functionality"

        # confirm text edit contains text
        self.main_window.text_edit.setText(text)
        self.assertEqual(self.main_window.text_edit.toPlainText(), text)

        self.assertIsNone(self.main_window.lesson_file)
        self.main_window.save_as_action.triggered.emit()
        self.assertIsNone(self.main_window.lesson_file)

    def test_save_as_selected_set_window_title_with_saved_filename(self):
        main_window.IN_TESTING = DialogAction.Selected

        text = "testing save as functionality"

        # confirm text edit contains text
        self.main_window.text_edit.setText(text)
        self.assertEqual(self.main_window.text_edit.toPlainText(), text)

        self.assertEqual(self.main_window.windowTitle(), f"{t_rex_typer.APPLICATION_NAME} - *")
        self.main_window.save_as_action.triggered.emit()
        self.assertEqual(self.main_window.windowTitle(), f"{t_rex_typer.APPLICATION_NAME} - {self.test_filename}")


class TestMainWindowLoadDictionaryAction(MainWindowCommon):

    def setUp(self):
        super().setUp()
        self.test_filename = f"{tempfile.gettempdir()}/test_on_load_dictionary.txt"

    def test_load_dictionary_action_text(self):
        self.assertEqual(
            self.main_window.load_dictionary_action.text(),
            "Load Dictionary...")

    def test_load_dictionary_action_has_tooltip(self):
        self.assertEqual(
            self.main_window.load_dictionary_action.toolTip(),
            "Load and replace the current dictionary")

    def test_load_dictionary_trigger_loads_dictionary_when_selected(self):
        main_window.IN_TESTING = DialogAction.Selected

        mock_load = unittest.mock.MagicMock()
        with unittest.mock.patch.object(
                t_rex_typer.translation_dict.TranslationDict, 'load', mock_load):
            self.main_window.load_dictionary_action.triggered.emit()
            mock_load.assert_called_once()

    def test_load_dictionary_trigger_cancel_does_not_change_dictionary_value(self):
        main_window.IN_TESTING = DialogAction.Canceled
        original_dictionary = self.main_window._dictionary

        self.main_window.load_dictionary_action.triggered.emit()

        self.assertEqual(self.main_window._dictionary, original_dictionary)


class TestMainWindowSettingsAction(MainWindowCommon):

    def test_settings_action_has_tooltip(self):
        self.assertIsNotNone(self.main_window.settings_action.toolTip())
        self.assertEqual(self.main_window.settings_action.toolTip(), "Open Settings Window")

    def test_settings_action_trigger_shows_raises_and_activates_settings_window(self):
        show_mock           = unittest.mock.MagicMock()
        raise_mock          = unittest.mock.MagicMock()
        activateWindow_mock = unittest.mock.MagicMock()

        self.main_window.settings_window.show           = show_mock
        self.main_window.settings_window.raise_         = raise_mock
        self.main_window.settings_window.activateWindow = activateWindow_mock

        self.main_window.settings_action.triggered.emit()

        show_mock.assert_called_once()
        raise_mock.assert_called_once()
        activateWindow_mock.assert_called_once()


class TestMainWindowExitAction(MainWindowCommon):

    def test_exit_action_has_tooltip(self):
        self.assertIsNotNone(self.main_window.exit_action.toolTip())

    def test_exit_action_trigger_closes_application(self):
        close_event_mock = unittest.mock.MagicMock()

        self.main_window.closeEvent = close_event_mock
        self.main_window.exit_action.triggered.emit()
        close_event_mock.assert_called_once()


class TestMainWindowOpenAction(MainWindowCommon):

    def setUp(self):
        super().setUp()
        self.test_filename = f"{tempfile.gettempdir()}/test_on_open.txt"
        if os.path.exists(self.test_filename):
            raise FileExistsError(f"'{self.test_filename}' should not exist. Delete it before running tests.")  # pragma: no cover

        with open(self.test_filename, 'w') as f:
            f.write("Test lesson text.")

    def test_open_action_has_shortcut(self):
        self.assertGreater(len(self.main_window.open_action.shortcuts()), 0)

    def test_open_action_has_tooltip(self):
        self.assertIsNotNone(self.main_window.open_action.toolTip())

    def test_open_trigger_sets_lesson_file_on_selected(self):
        main_window.IN_TESTING = DialogAction.Selected

        self.assertIsNone(self.main_window.lesson_file)
        self.main_window.open_action.triggered.emit()

        self.assertEqual(self.main_window.lesson_file, self.test_filename)

    def test_open_trigger_sets_lesson_directory_setting_on_selected(self):
        main_window.IN_TESTING = DialogAction.Selected

        self.assertEqual(
            t_rex_typer.main_window.SETTINGS.lesson_directory,
            t_rex_typer.main_window.SETTINGS['lesson_directory']._default)
        self.main_window.open_action.triggered.emit()

        self.assertEqual(
            t_rex_typer.main_window.SETTINGS.lesson_directory,
            os.path.dirname(self.test_filename))

    def test_open_trigger_doesnt_set_lesson_file_on_cancel(self):
        main_window.IN_TESTING = DialogAction.Canceled

        self.assertIsNone(self.main_window.lesson_file)
        self.main_window.open_action.triggered.emit()
        self.assertIsNone(self.main_window.lesson_file)

    def test_open_trigger_sets_text_edit_on_selected(self):
        main_window.IN_TESTING = DialogAction.Selected

        self.assertEqual(self.main_window.text_edit.toPlainText(), '')
        self.main_window.open_action.triggered.emit()
        self.assertEqual(self.main_window.text_edit.toPlainText(), "Test lesson text.")

    def test_open_error_creates_message_box(self):
        main_window.IN_TESTING = DialogAction.Selected

        def induce_error(*arg, **kwargs):
            raise FileNotFoundError("Test induced error")

        built_in_open = open
        main_window.open = induce_error

        # error is handled
        self.main_window.open_action.triggered.emit()

        # assumes that the message box gets shown
        self.assertIsInstance(self.main_window.message_box, QtWidgets.QMessageBox)
        self.assertEqual(self.main_window.message_box.windowTitle(), "T-Rex Terror")
        self.assertEqual(self.main_window.message_box.icon(), QtWidgets.QMessageBox.Warning)
        self.assertEqual(self.main_window.message_box.text(), f"Could not open file:\n\n{self.test_filename}")
        self.assertEqual(self.main_window.message_box.informativeText(), "Address the error below and try again.")
        self.assertNotEqual(self.main_window.message_box.detailedText(), "")

        main_window.open = built_in_open

    def test_opening_file_sets_window_title_to_file_name(self):
        main_window.IN_TESTING = DialogAction.Selected

        self.assertEqual(self.main_window.windowTitle(), t_rex_typer.APPLICATION_NAME)
        self.main_window.open_action.triggered.emit()
        self.assertEqual(
            self.main_window.windowTitle(),
            t_rex_typer.APPLICATION_NAME + ' - ' + self.test_filename)


class TestSettingsWindow(SettingsCommon):

    def setUp(self):
        super().setUp()
        self.settings_window = self.main_window.settings_window

    def test_is_qwidget(self):
        self.assertIsInstance(self.settings_window, QtWidgets.QWidget)

    def test_has_nonzero_minimum_width(self):
        self.assertEqual(self.settings_window.minimumWidth(), 600)

    def test_window_title(self):
        self.assertEqual(self.settings_window.windowTitle(), f"Settings")

    def test_window_flags(self):
        # https://stackoverflow.com/a/40007740
        flags = self.settings_window.windowFlags()
        self.assertTrue(int(flags & ~QtCore.Qt.WindowContextHelpButtonHint))
        self.assertTrue(int(flags & QtCore.Qt.WindowStaysOnTopHint))

    def test_shows_latest_settings(self):
        mock_set = unittest.mock.MagicMock()
        t_rex_typer.main_window.SETTINGS.__dict__['set'] = mock_set
        self.assertIs(t_rex_typer.main_window.SETTINGS.set, mock_set)

        self.main_window.settings_action.triggered.emit()

        mock_set.assert_called_once()

        # NOTE: check doesn't load main_window geometry etc. That is
        # not user facing and should only be loaded on application
        # open.
        called_with = mock_set.call_args[0][0]
        was_called_with_main_window_settings = [k[:12] == 'main_window_' for k in called_with]
        self.assertFalse(any(was_called_with_main_window_settings))

    def test_not_modified_when_first_opened(self):
        self.assertFalse(self.settings_window._modified)
        self.main_window.settings_action.triggered.emit()
        self.assertFalse(self.settings_window._modified)

    def test_has_button_box(self):
        self.assertIsInstance(self.settings_window.button_box, QtWidgets.QDialogButtonBox)

    def test_has_ok_button(self):
        self.assertIsInstance(self.settings_window.ok_button, QtWidgets.QPushButton)
        self.assertIn(self.settings_window.ok_button, self.settings_window.button_box.buttons())

    def test_ok_button_tooltip(self):
        self.assertEqual(
            self.settings_window.ok_button.toolTip(),
            "Save changes and close window")

    def test_has_cancel_button(self):
        self.assertIsInstance(self.settings_window.cancel_button, QtWidgets.QPushButton)
        self.assertIn(self.settings_window.cancel_button, self.settings_window.button_box.buttons())

    def test_cancel_button_tooltip(self):
        self.assertEqual(
            self.settings_window.cancel_button.toolTip(),
            "Close window without applying changes")

    def test_has_apply_button(self):
        self.assertIsInstance(self.settings_window.apply_button, QtWidgets.QPushButton)
        self.assertIn(self.settings_window.apply_button, self.settings_window.button_box.buttons())

    def test_apply_button_tooltip(self):
        self.assertEqual(
            self.settings_window.apply_button.toolTip(),
            "Save changes")

    def test_apply_button_disabled_initially(self):
        self.assertFalse(self.settings_window.apply_button.isEnabled())

    def test_has_restore_defaults_link_label(self):
        self.assertIsInstance(
            self.settings_window.restore_defaults_link,
            QtWidgets.QLabel)

    def test_restore_defaults_link_text(self):
        self.assertEqual(
            self.settings_window.restore_defaults_link.text(),
            '<a href=".">Restore defaults</a>')

    def test_restore_defaults_link_tooltip(self):
        self.assertEqual(
            self.settings_window.restore_defaults_link.toolTip(),
            "Restore default settings")

    def test_restore_defaults_link_sets_settings_to_default_values_when_clicked(self):
        self.settings_window.wpm_threshold_spinbox.setValue(42)
        self.assertEqual(self.settings_window.wpm_threshold_spinbox.value(), 42)
        self.assertNotEqual(
            self.settings_window.wpm_threshold_spinbox.value(),
            t_rex_typer.main_window.SETTINGS['wpm_threshold']._default)

        self.settings_window.restore_defaults_link.linkActivated.emit(
            self.settings_window.restore_defaults_link.text())

        self.assertEqual(
            self.settings_window.wpm_threshold_spinbox.value(),
            t_rex_typer.main_window.SETTINGS['wpm_threshold']._default)

    def test_restore_defaults_link_sets_private_modified(self):
        self.assertFalse(self.settings_window._modified)
        self.settings_window.restore_defaults_link.linkActivated.emit(
            self.settings_window.restore_defaults_link.text())
        self.assertTrue(self.settings_window._modified)

    def test_has_modified_property(self):
        self.assertEqual(self.settings_window._modified, False)
        self.assertIsInstance(
            getattr(type(self.settings_window), 'modified', None),
            property)

    def test_modified_setter_sets_private_modified(self):
        self.assertFalse(self.settings_window._modified)
        self.settings_window.modified = True
        self.assertTrue(self.settings_window._modified)
        self.settings_window.modified = False
        self.assertFalse(self.settings_window._modified)

    def test_modified_getter_gets_private_modified(self):
        self.assertFalse(self.settings_window._modified)
        self.assertEqual(
            self.settings_window.modified,
            self.settings_window._modified)

        self.settings_window._modified = True

        self.assertEqual(
            self.settings_window.modified,
            self.settings_window._modified)

    def test_modified_must_be_bool(self):
        try:
            self.settings_window.modified = "banana"
        except TypeError:
            pass
        else:
            raise AssertionError("SettingsWindow.modified must raise TypeError when not a bool")

    def test_when_modified_title_has_star(self):
        self.assertEqual(self.settings_window.modified, False)
        self.assertNotIn("*", self.settings_window.windowTitle())

        self.settings_window.modified = True

        self.assertIn("*", self.settings_window.windowTitle())

    def test_when_not_modified_title_has_no_star(self):
        self.settings_window.modified = True
        self.assertEqual(self.settings_window.modified, True)
        self.assertIn("*", self.settings_window.windowTitle())

        self.settings_window.modified = False

        self.assertNotIn("*", self.settings_window.windowTitle())

    def test_apply_button_enabled_when_modified(self):
        self.assertFalse(self.settings_window._modified)
        self.assertFalse(self.settings_window.apply_button.isEnabled())

        # test property setter
        self.settings_window.modified = True

        self.assertTrue(self.settings_window._modified)
        self.assertTrue(self.settings_window.apply_button.isEnabled())

    def test_when_not_modified_apply_button_disabled(self):
        self.settings_window._modified = True
        self.settings_window.apply_button.setEnabled(True)

        # test property setter
        self.settings_window.modified = False

        self.assertFalse(self.settings_window._modified)
        self.assertFalse(self.settings_window.apply_button.isEnabled())

    def test_apply_button_saves_settings_to_disk(self):
        mock_write = unittest.mock.MagicMock()
        t_rex_typer.main_window.SETTINGS.__dict__['write'] = mock_write
        self.assertIs(t_rex_typer.main_window.SETTINGS.write, mock_write)

        self.settings_window.apply_button.clicked.emit()

        mock_write.assert_called_once()

    def test_apply_button_makes_modified_false(self):
        self.settings_window._modified = True
        self.assertTrue(self.settings_window._modified)

        self.settings_window.apply_button.clicked.emit()

        self.assertFalse(self.settings_window._modified)

    def test_cancel_button_closes_window_without_saving_changes(self):
        mock_close_event = unittest.mock.MagicMock()
        # NOTE: for some reason, mocking self.settings_window.close
        # didn't work, even though I was able to trace and step into
        # it when overriding close.
        self.settings_window.closeEvent = mock_close_event

        mock_write = unittest.mock.MagicMock()
        t_rex_typer.main_window.SETTINGS.__dict__['write'] = mock_write
        self.assertIs(t_rex_typer.main_window.SETTINGS.write, mock_write)

        self.settings_window.button_box.rejected.emit()

        mock_close_event.assert_called_once()
        mock_write.assert_not_called()

    def test_ok_button_enabled_when_modified(self):
        self.assertFalse(self.settings_window._modified)
        self.assertFalse(self.settings_window.ok_button.isEnabled())

        # test property setter
        self.settings_window.modified = True

        self.assertTrue(self.settings_window._modified)
        self.assertTrue(self.settings_window.ok_button.isEnabled())

    def test_when_not_modified_ok_button_disabled(self):
        self.settings_window._modified = True
        self.settings_window.ok_button.setEnabled(True)

        # test property setter
        self.settings_window.modified = False

        self.assertFalse(self.settings_window._modified)
        self.assertFalse(self.settings_window.ok_button.isEnabled())

    def test_ok_button_saves_settings_to_disk(self):
        mock_write = unittest.mock.MagicMock()
        t_rex_typer.main_window.SETTINGS.__dict__['write'] = mock_write
        self.assertIs(t_rex_typer.main_window.SETTINGS.write, mock_write)

        self.settings_window.ok_button.clicked.emit()

        mock_write.assert_called_once()

    def test_ok_button_makes_modified_false(self):
        self.settings_window._modified = True
        self.assertTrue(self.settings_window._modified)

        self.settings_window.ok_button.clicked.emit()

        self.assertFalse(self.settings_window._modified)

    def test_ok_button_closes_window(self):
        mock_close = unittest.mock.MagicMock()
        self.settings_window.close = mock_close

        self.settings_window.ok_button.clicked.emit()

        mock_close.assert_called_once()

    def test_has_wpm_threshold_label(self):
        self.assertIsInstance(self.settings_window.wpm_threshold_label, QtWidgets.QLabel)

    def test_wpm_threshold_label_text(self):
        self.assertEqual(self.settings_window.wpm_threshold_label.text(), "WPM Miss Threshold:")

    def test_wpm_threshold_label_tooltip(self):
        self.assertEqual(
            self.settings_window.wpm_threshold_label.toolTip(),
            "Count multi-strokes slower than this as a miss")

    def test_has_wpm_threshold_spinbox(self):
        self.assertIsInstance(self.settings_window.wpm_threshold_spinbox, QtWidgets.QSpinBox)

    def test_wpm_threshold_spinbox_tooltip(self):
        self.assertEqual(
            self.settings_window.wpm_threshold_spinbox.toolTip(),
            "Words per minute")

    def test_wpm_threshold_spinbox_range(self):
        self.assertEqual(self.settings_window.wpm_threshold_spinbox.minimum(), 0)
        self.assertEqual(self.settings_window.wpm_threshold_spinbox.maximum(), 400)

    def test_wpm_threshold_spinbox_change_sets_modified_true(self):
        self.assertFalse(self.settings_window._modified)
        self.settings_window.wpm_threshold_spinbox.valueChanged.emit(31)
        self.assertTrue(self.settings_window._modified)

    def test_has_lesson_directory_label(self):
        self.assertIsInstance(self.settings_window.lesson_directory_label, QtWidgets.QLabel)

    def test_lesson_directory_label(self):
        self.assertEqual(
            self.settings_window.lesson_directory_label.text(),
            "Lesson Directory:")

    def test_lesson_directory_label_tooltip(self):
        self.assertEqual(
            self.settings_window.lesson_directory_label.toolTip(),
            "Lesson directory")

    def test_has_lesson_directory_line_edit(self):
        self.assertIsInstance(
            self.settings_window.lesson_directory_line_edit,
            QtWidgets.QLineEdit)

    def test_lesson_directory_line_edit_text_change_sets_modified_true(self):
        self.assertFalse(self.settings_window._modified)
        self.settings_window.lesson_directory_line_edit.textChanged.emit("the change")
        self.assertTrue(self.settings_window._modified)

    def test_has_dictionary_directory_label(self):
        self.assertIsInstance(self.settings_window.dictionary_directory_label, QtWidgets.QLabel)

    def test_dictionary_directory_label(self):
        self.assertEqual(
            self.settings_window.dictionary_directory_label.text(),
            "Dictionary Directory:")

    def test_dictionary_directory_label_tooltip(self):
        self.assertEqual(
            self.settings_window.dictionary_directory_label.toolTip(),
            "Plover dictionary directory")

    def test_has_dictionary_directory_line_edit(self):
        self.assertIsInstance(
            self.settings_window.dictionary_directory_line_edit,
            QtWidgets.QLineEdit)

    def test_dictionary_directory_line_edit_text_change_sets_modified_true(self):
        self.assertFalse(self.settings_window._modified)
        self.settings_window.dictionary_directory_line_edit.textChanged.emit("new/dictionary/directory")
        self.assertTrue(self.settings_window._modified)


class TestAboutWindow(unittest.TestCase):

    def setUp(self):
        # tests may change the value of IN_TESTING
        main_window.IN_TESTING = True
        self.about_window = main_window.AboutWindow()

    def test_about_window_is_qwidget(self):
        self.assertIsInstance(self.about_window, QtWidgets.QWidget)

    def test_about_window_title(self):
        self.assertEqual(
            self.about_window.windowTitle(),
            f"About {t_rex_typer.APPLICATION_NAME}")

    def test_window_flags(self):
        # https://stackoverflow.com/a/40007740
        flags = self.about_window.windowFlags()
        self.assertTrue(int(flags & QtCore.Qt.WindowTitleHint))
        self.assertTrue(int(flags & QtCore.Qt.WindowMinMaxButtonsHint))
        self.assertTrue(int(flags & QtCore.Qt.WindowCloseButtonHint))
        self.assertTrue(int(flags & ~QtCore.Qt.WindowContextHelpButtonHint))
        self.assertTrue(int(flags & QtCore.Qt.WindowStaysOnTopHint))

    def test_has_icon_label(self):
        self.assertIsInstance(self.about_window.icon_label, QtWidgets.QLabel)

    def test_icon_label_shows_application_icon(self):
        self.assertEqual(
            self.about_window.icon_label.pixmap().cacheKey(),
            t_rex_typer.APPLICATION_PIXMAP.cacheKey())

    def test_has_body_title_attribute(self):
        self.assertIsInstance(self.about_window.body_title, QtWidgets.QLabel)

    def test_body_title_displays_header(self):
        self.assertEqual(self.about_window.body_title.text(), f'<h1>About</h1>')

    def test_has_body_attribute(self):
        self.assertIsInstance(self.about_window.body, QtWidgets.QLabel)

    def test_body_displays_credits(self):
        expected_body_text = (
            f'<p>The {t_rex_typer.APPLICATION_NAME} is made by Matt Trzcinski.</p>'
            f'<hr>'
            f'<p>'
            f'<a href="https://icons8.com/icon/5vV_csnCe5Q2/kawaii-dinosaur">kawaii-dinosaur</a> and '
            f'<a href="https://icons8.com/icon/87796/keyboard">keyboard</a> by <a href="https://icons8.com">Icons8</a>.'
            f'</p>'
        )

        self.assertEqual(self.about_window.body.text(), expected_body_text)

    def test_body_has_clickable_links(self):
        self.assertTrue(self.about_window.body.openExternalLinks())

    def test_body_selectable_by_mouse(self):
        body_flags = self.about_window.body.textInteractionFlags()
        self.assertTrue(int(body_flags & QtCore.Qt.TextSelectableByMouse))

    def test_hover_over_link_shows_URL(self):
        show_text_mock = unittest.mock.MagicMock()

        QtWidgets.QToolTip.showText = show_text_mock

        self.about_window.body.linkHovered.emit("test_hover_over_link_shows_URL")

        # PySide2.QtWidgets.QToolTip.showText(pos, text[, w=None])
        # Don't care about the position
        show_text_mock.assert_called_once_with(unittest.mock.ANY, "test_hover_over_link_shows_URL")

    def test_hover_over_nonlink_hides_URL(self):
        hide_text_mock = unittest.mock.MagicMock()

        QtWidgets.QToolTip.hideText = hide_text_mock

        self.about_window.body.linkHovered.emit(None)
        hide_text_mock.assert_called_once()

    def test_close_event_hides_window(self):
        hide_mock = unittest.mock.MagicMock()

        self.about_window.hide = hide_mock
        self.about_window.close()
        hide_mock.assert_called_once()
