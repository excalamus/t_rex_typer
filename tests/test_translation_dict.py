# test_translation_dict.py
#
# Run tests with
#
#   python3 -m unittest discover tests/ --failfast --quiet

import os
import types
import unittest
import tempfile
from PySide2 import QtCore, QtWidgets, QtGui, QtTest
from t_rex_typer import translation_dict


if not QtWidgets.QApplication.instance():
    APP = QtWidgets.QApplication([])  # pragma: no cover


class TestTranslationDictArguments(unittest.TestCase):

    def test_takes_optional_list_of_paths_to_json_plover_dicts(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_plover_dict1 = os.path.join(temp_dir, "plover_dict1.json")
            temp_plover_dict2 = os.path.join(temp_dir, "plover_dict2.json")

            with open(temp_plover_dict1, 'w', encoding='utf-8') as f:
                source_plover_dict1 = "{\n\"#*E\": \"{>}{&e}\",\n\"#*U\": \"{>}{&u}\",\n\"#-D\": \"{^ed}\"\n}"
                f.write(source_plover_dict1)

            with open(temp_plover_dict2, 'w', encoding='utf-8') as f:
                source_plover_dict2 = "{\n\"#-Z\": \"00\",\n\"#EUD/KWROPL\": \"idiom\",\n\"#K*\": \"{>}{&k}\"\n}"
                f.write(source_plover_dict2)

            # NOTE: dict1 appears before dict2
            dict_list = [temp_plover_dict1, temp_plover_dict2]

            tdict = translation_dict.TranslationDict(plover_dicts=dict_list)

            self.assertEqual(
                tdict._data,
                {"#*E": "{>}{&e}",
                 "#*U": "{>}{&u}",
                 "#-D": "{^ed}",
                 "#-Z": "00",
                 "#EUD/KWROPL": "idiom",
                 "#K*": "{>}{&k}"}
            )


class TestTranslationDictContainerEmulation(unittest.TestCase):

    def setUp(self):
        self.tdict = translation_dict.TranslationDict()

    def test_has_internal_dict_storage(self):
        self.assertIsInstance(self.tdict._data, dict)

    def test_supports_item_assignment(self):
        # has dict-like setting
        self.assertEqual(self.tdict._data, {})
        self.tdict['test_set_value'] = 'set a value'
        self.assertEqual(self.tdict._data, {'test_set_value': 'set a value'})

    def test_is_subscriptable(self):
        # has dict-like lookup
        self.tdict._data = {'test_get_value_by_lookup': 'got value by lookup'}
        self.assertEqual(self.tdict._data, {'test_get_value_by_lookup': 'got value by lookup'})
        self.assertEqual(self.tdict['test_get_value_by_lookup'], 'got value by lookup')

    def test_get_method(self):
        self.tdict._data = {'test_get_value': 'got value using get'}
        self.assertEqual(self.tdict._data, {'test_get_value': 'got value using get'})
        self.assertEqual(self.tdict.get('test_get_value'), 'got value using get')

    def test_get_method_default(self):
        self.assertEqual(self.tdict._data, {})
        self.assertEqual(self.tdict.get('test_get_default', 'got default value'), 'got default value')

    def test_pop_removes_key_and_returns_associated_value(self):
        self.tdict._data = {'value_to_pop': 'popped value'}
        self.assertEqual(self.tdict._data, {'value_to_pop': 'popped value'})
        popped = self.tdict.pop('value_to_pop')
        self.assertEqual(popped, 'popped value')

    def test_pop_takes_takes_a_default(self):
        self.assertEqual(self.tdict._data, {})
        popped = self.tdict.pop('pop_default', 'default value')
        self.assertEqual(popped, 'default value')

    def test_is_iterable(self):
        self.tdict._data = {'first': 1, 'second': 2, 'third': 3}
        items = list(self.tdict)
        self.assertEqual(set(items), set(['first', 'second', 'third']))

    def test_keys_method(self):
        self.tdict._data = {'test_keys_key1': 'test_keys value 1', 'test_keys_key2': 'test_keys value 2'}
        self.assertEqual(list(self.tdict.keys()), ['test_keys_key1', 'test_keys_key2'])

    def test_values_method(self):
        self.tdict._data = {'test_values_key1': 'test_values value 1', 'test_values_key2': 'test_values value 2'}
        self.assertEqual(list(self.tdict.values()), ['test_values value 1', 'test_values value 2'])

    def test_items_method(self):
        self.tdict._data = {'test_items_key1': 'test_items_value 1', 'test_items_key2': 'test_items_value 2'}
        self.assertEqual(list(self.tdict.items()), [('test_items_key1', 'test_items_value 1'), ('test_items_key2','test_items_value 2')])

    def test_membership_support(self):
        self.tdict._data = {'is_in': 'value is in'}
        self.assertTrue('is_in' in self.tdict)
        self.assertTrue(self.tdict.__contains__('is_in'))

    def test_len_support(self):
        self.tdict._data = {'len test 1': 'len test value 1',
                            'len test 2': 'len test value 2',
                            'len test 3': 'len test value 3',}

        self.assertEqual(len(self.tdict), 3)

    def test_prints_repr(self):
        values = {'repr 1': 'value 1', 'repr 2': 'value 2', 'repr 3': 'value 3',}
        self.tdict._data = values
        self.assertEqual(self.tdict.__repr__(), str(values))


class TestTranslationDict(unittest.TestCase):

    def setUp(self):
        self.tdict = translation_dict.TranslationDict()

    def test_static_load__is_static_method(self):
        self.assertIsInstance(self.tdict.load_, types.FunctionType)

    def test_static_load__returns_concatenated_plover_dictionaries(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_plover_dict1 = os.path.join(temp_dir, "plover_dict1.json")
            temp_plover_dict2 = os.path.join(temp_dir, "plover_dict2.json")

            with open(temp_plover_dict1, 'w', encoding='utf-8') as f:
                source_plover_dict1 = "{\n\"#*E\": \"{>}{&e}\",\n\"#*U\": \"{>}{&u}\",\n\"#-D\": \"{^ed}\"\n}"
                f.write(source_plover_dict1)

            with open(temp_plover_dict2, 'w', encoding='utf-8') as f:
                source_plover_dict2 = "{\n\"#-Z\": \"00\",\n\"#EUD/KWROPL\": \"idiom\",\n\"#K*\": \"{>}{&k}\"\n}"
                f.write(source_plover_dict2)

            # NOTE: dict1 appears before dict2
            dict_list = [temp_plover_dict1, temp_plover_dict2]
            self.assertEqual(

                self.tdict.load_(dict_list),
                {"#*E": "{>}{&e}",
                 "#*U": "{>}{&u}",
                 "#-D": "{^ed}",
                 "#-Z": "00",
                 "#EUD/KWROPL": "idiom",
                 "#K*": "{>}{&k}"}
            )

    def test_load_sets_internal_dict(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_plover_dict1 = os.path.join(temp_dir, "plover_dict1.json")
            temp_plover_dict2 = os.path.join(temp_dir, "plover_dict2.json")

            with open(temp_plover_dict1, 'w', encoding='utf-8') as f:
                source_plover_dict1 = "{\n\"#*E\": \"{>}{&e}\",\n\"#*U\": \"{>}{&u}\",\n\"#-D\": \"{^ed}\"\n}"
                f.write(source_plover_dict1)

            with open(temp_plover_dict2, 'w', encoding='utf-8') as f:
                source_plover_dict2 = "{\n\"#-Z\": \"00\",\n\"#EUD/KWROPL\": \"idiom\",\n\"#K*\": \"{>}{&k}\"\n}"
                f.write(source_plover_dict2)

            # NOTE: dict1 appears before dict2
            dict_list = [temp_plover_dict1, temp_plover_dict2]
            self.tdict.load(dict_list)
            self.assertEqual(
                self.tdict._data,
                {"#*E": "{>}{&e}",
                 "#*U": "{>}{&u}",
                 "#-D": "{^ed}",
                 "#-Z": "00",
                 "#EUD/KWROPL": "idiom",
                 "#K*": "{>}{&k}"}
            )

    def test_get_stroke_indices(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_plover_dict1 = os.path.join(temp_dir, "plover_dict1.json")

            with open(temp_plover_dict1, 'w', encoding='utf-8') as f:
                source_plover_dict1 = (
                    "{"
                    "\n\"E\": \"banana\","
                    "\n\"U\": \"apple\","
                    "\n\"D\": \"Banana\""
                    "}"
                )
                f.write(source_plover_dict1)

            dict_list = [temp_plover_dict1]
            self.tdict.load(dict_list)
            self.assertEqual(
                self.tdict._data,
                {
                    "E": "banana",  # 0
                    "U": "apple",   # 1
                    "D": "Banana",  # 2
                 }
            )

            # NOTE: case sensitive
            self.assertEqual(self.tdict._get_stroke_indices("banana"), [0])

    def test_get_strokes(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_plover_dict1 = os.path.join(temp_dir, "plover_dict1.json")

            with open(temp_plover_dict1, 'w', encoding='utf-8') as f:
                source_plover_dict1 = "{\n\"TKUPL/KAT\": \"duplicate\",\n\"AP/-L\": \"apple\",\n\"TKAOUP/KAT\": \"duplicate\"}"
                f.write(source_plover_dict1)

            dict_list = [temp_plover_dict1]
            self.tdict.load(dict_list)
            self.assertEqual(
                self.tdict._data,
                {
                    "TKUPL/KAT" : "duplicate",
                    "AP/-L"     : "apple",
                    "TKAOUP/KAT": "duplicate",
                 }
            )

            self.assertEqual(self.tdict.get_strokes("duplicate"), ["TKUPL/KAT", "TKAOUP/KAT"])

    def test_get_strokes_sorted(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_plover_dict1 = os.path.join(temp_dir, "plover_dict1.json")

            with open(temp_plover_dict1, 'w', encoding='utf-8') as f:

                source_plover_dict1 = (
                    "{"
                    "\n\"AP/-L\": \"apple\","
                    "\n\"PWA/TPHA/TPHA\": \"banana\","
                    "\n\"PA/TPHA/TPHA\": \"banana\","
                    "\n\"PWAPB/TPHA\": \"banana\""
                    "}"
                )

                f.write(source_plover_dict1)

            dict_list = [temp_plover_dict1]
            self.tdict.load(dict_list)
            self.assertEqual(
                self.tdict._data,
                {
                    "AP/-L"        : "apple",
                    "PWA/TPHA/TPHA": "banana",
                    "PA/TPHA/TPHA" : "banana",
                    "PWAPB/TPHA"   : "banana",
                }
            )

            self.assertEqual(
                self.tdict.get_strokes("banana", sorted=True),
                [
                    'PWAPB/TPHA',
                    'PA/TPHA/TPHA',
                    'PWA/TPHA/TPHA',
                ]
            )

    def test_split_into_strokable_units(self):
        text = "This is a 'test'."
        self.assertEqual(
            self.tdict.split_into_strokable_units(text),
            ['This', 'is', 'a', "'", 'test', "'", '.']
        )

    def test_split_into_strokable_units_is_static_method(self):
        self.assertIsInstance(self.tdict.split_into_strokable_units, types.FunctionType)

    def test_translate(self):
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_plover_dict1 = os.path.join(temp_dir, "plover_dict1.json")

            with open(temp_plover_dict1, 'w', encoding='utf-8') as f:

                # "\n\"\": \"\","

                source_plover_dict1 = (
                    "{"
                    "\n\"TPAULS\"  : \"falls\","
                    "\n\"STAEUS\"  : \"stays\","
                    "\n\"TPH\"     : \"in\","
                    "\n\"PHAEUPBL\": \"mainly\","
                    "\n\"OPB\"     : \"on\","
                    "\n\"PHRAEUPB\": \"plain\","
                    "\n\"-T\"      : \"the\","
                    "\n\"RAEUPB\"  : \"rain\","
                    "\n\"SPAEUPB\" : \"Spain\","
                    "\n\"PR-D\"    : \".\""
                    "}"
                )

                f.write(source_plover_dict1)

            dict_list = [temp_plover_dict1]
            self.tdict.load(dict_list)
            self.assertEqual(
                self.tdict._data,
                {
                    '-T'      : 'the',
                    'STAEUS'  : 'stays',
                    'OPB'     : 'on',
                    'PHAEUPBL': 'mainly',
                    'PHRAEUPB': 'plain',
                    'PR-D'    : '.',
                    'RAEUPB'  : 'rain',
                    'SPAEUPB' : 'Spain',
                    'TPAULS'  : 'falls',
                    'TPH'     : 'in'
                }
            )

        text = "the rain in Spain stays mainly in the plain."
        self.assertEqual(
            self.tdict.translate(text),
            ['-T','RAEUPB','TPH','SPAEUPB','STAEUS','PHAEUPBL','TPH','-T','PHRAEUPB','PR-D']
        )
