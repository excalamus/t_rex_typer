# test_text_label.py
#
# Run tests with
#
#   python3 -m unittest discover tests/ --failfast --quiet

import unittest
from PySide2 import QtCore, QtWidgets, QtGui, QtTest
from t_rex_typer import text_label


if not QtWidgets.QApplication.instance():
    QtWidgets.QApplication([])  # pragma: no cover


class TestTextLabelArguments(unittest.TestCase):

    def test_optional_text_argument(self):
        tlabel = text_label.TextLabel()
        self.assertEqual(tlabel.toPlainText(), "")

    def test_text_argument_sets_label_text(self):
        tlabel = text_label.TextLabel(text="Test text")
        self.assertEqual(tlabel.toPlainText(), "Test text")

    def test_optional_parent_argument(self):
        widget = QtWidgets.QWidget()
        tlabel = text_label.TextLabel(parent=widget)
        self.assertEqual(tlabel.parent(), widget)

    def test_viewport_base_color_takes_on_color_of_the_parent_window(self):
        # QPalette.Window
        #
        #     A general background color.  (QPalette.Background is
        #     obsolete).
        #
        # QPalette.Base
        #
        #     Used mostly as the background color for text entry
        #     widgets, but can also be used for other painting - such
        #     as the background of combobox drop down lists and
        #     toolbar handles. It is usually white or another light
        #     color.

        # We want the TextLabel, which is a TextEdit, to look like a
        # QLabel.  The viewport is white by default.  It needs to
        # match the color of the parent.  QPalette.Base is what
        # controls the color of the viewport.

        parent = QtWidgets.QWidget()
        pal    = parent.palette()
        pal.setColor(QtGui.QPalette.Window, QtCore.Qt.red)
        parent.setPalette(pal)
        parent_window_color = parent.palette().window().color()
        self.assertEqual(parent_window_color, QtCore.Qt.red)

        tlabel = text_label.TextLabel(parent=parent)
        tlabel_viewport_base_color = tlabel.viewport().palette().base().color()

        self.assertEqual(tlabel_viewport_base_color, parent_window_color)

    def test_has_the_default_light_gray_color_when_parent_not_passed_in(self):
        tlabel       = text_label.TextLabel()
        tlabel_color = tlabel.palette().window().color()
        default_gray = QtWidgets.QWidget().palette().window().color()
        self.assertEqual(tlabel_color, default_gray)


class TestTextLabel(unittest.TestCase):

    def setUp(self):
        self.tlabel = text_label.TextLabel()

    def test_is_a_text_edit(self):
        self.assertIsInstance(self.tlabel, QtWidgets.QTextEdit)

    def test_is_read_only(self):
        self.assertEqual(self.tlabel.isReadOnly(), True)

    def test_has_no_horizontal_scroll_bars(self):
        self.assertEqual(self.tlabel.horizontalScrollBarPolicy(), QtCore.Qt.ScrollBarAlwaysOff)

    def test_has_no_vertical_scroll_bars(self):
        self.assertEqual(self.tlabel.verticalScrollBarPolicy(), QtCore.Qt.ScrollBarAlwaysOff)

    def test_does_not_wrap_text(self):
        self.assertEqual(self.tlabel.lineWrapMode(), QtWidgets.QTextEdit.NoWrap)

    def test_has_no_frame(self):
        self.assertEqual(self.tlabel.frameShape(), QtWidgets.QFrame.NoFrame)

    def test_has_no_text_margins(self):
        self.assertEqual(self.tlabel.document().documentMargin(), 0.0)

    def test_widget_height_sizes_according_to_the_text_height(self):
        font_metrics = QtGui.QFontMetrics(self.tlabel.font())
        self.assertEqual(self.tlabel.height(), font_metrics.height())
